﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputIndicator : MonoBehaviour
{
    public Sprite mouse;
    public Sprite joystick;
    public CrosshairManager crosshairManager;
    Image imageRenderer;
    public RectTransform image;

    void Start()
    {
        imageRenderer = GetComponent<Image>();

    }

    // Update is called once per frame
    void Update()
    {
        if(crosshairManager.inputMode == InputMode.joystick)
        {
            imageRenderer.sprite = joystick;
            image.localScale = new Vector3(1f, 1f, 1f);
        }
        else
        {
            image.localScale = new Vector3(1f, 1.5f, 1f);
            imageRenderer.sprite = mouse;
            
        }
    }
}
