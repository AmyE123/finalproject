﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseMove : MonoBehaviour
{
    public Vector3 mousePos;
    public bool hasMovedThisFrame;

    // Start is called before the first frame update
    void Start()
    {
        mousePos = Input.mousePosition;

    }

    // Update is called once per frame
    void Update()
    {
        if(mousePos != Input.mousePosition)
        {
            hasMovedThisFrame = true;
            mousePos = Input.mousePosition;
        }
        else
        {
            hasMovedThisFrame = false;
        }
    }
}
