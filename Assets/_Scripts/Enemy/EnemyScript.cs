﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour
{
    public Transform player; 
    public bool foundPlayer;
    public float findDistance = 100f;
    public float shootingDistance = 50f;
    public GameObject enemyFire;
    public float enemyMoveSpeed;
    public float enemyRotate = 0.05f;
    public int enemyHealth = 100;

    [Header("Timer properties")]
    public float timeToStop = 3f;


    public void TakeDamage(int damageGiven)
    {
        transform.localScale *= 0.9f;
        Destroy(gameObject);
        enemyHealth = enemyHealth - damageGiven;
    }

    public void Start()
    {
        player = GameObject.Find("PlayerParent").transform;
        EnemyManager.Register(this);
    }

    public void Update()
    {
        if (!foundPlayer)
        {
            IdleFly();
            FindDragon();
        }
        else
        {
            LookAtDragon();
            MoveTowardDragon();
            ShootDragon();
        }
    }

    public void IdleFly()
    {
        //There is no idleFly at the moment
    }

    public void FindDragon()
    {
        float distance = Vector3.Distance(player.position, transform.position);
        if(distance <= findDistance)
        {
            foundPlayer = true;
        }
    }

    public void LookAtDragon()
    {
        Quaternion targetRotation = Quaternion.LookRotation(player.position - transform.position, Vector3.up);
        transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, enemyRotate);

    }

    public void MoveTowardDragon()
    {
        transform.position += transform.forward * Time.deltaTime * enemyMoveSpeed;
        Vector3 targetPosition = new Vector3(transform.position.x, player.position.y, transform.position.z);
        transform.position = Vector3.Lerp(transform.position, targetPosition, 0.01f);
    }

    public void ShootDragon()
    {
        Vector3 directionToPlayer = (player.position - transform.position).normalized;
        bool isPlayerInfront = Vector3.Dot(transform.forward, directionToPlayer) > 0.9f;
        float distance = Vector3.Distance(player.position, transform.position);
        if(distance <= shootingDistance && isPlayerInfront && timeToStop <= 0)
        {
            GameObject newEnemyFire = Instantiate(enemyFire);
            newEnemyFire.transform.position = transform.position;
            newEnemyFire.GetComponent<EnemyFire>().player = player.GetComponent<PlayerController>();
            timeToStop = 3f;
        }
        timeToStop -= Time.deltaTime;
    }

    void OnDestroy()
    {
        EnemyManager.Deregister(this);
    }
}
