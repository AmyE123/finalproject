﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFire : MonoBehaviour
{
    [Header("Player script reference")]
    public PlayerController player;

    [Header("Enemy fire speed")]
    public float moveSpeed;

    [Header("Damage")]
    public int damageDealt;

    [Header("Time")]
    public float timeToFollow = 5f;
    public float timeToDestroy = 10f;
    public float trackingSpeed = 5f;

    void Update()
    {
        //This checks if there is a player to fire at
        if (player != null)
        {
            MoveTowardsPlayer();
        }
    }

    void MoveTowardsPlayer()
    {
        timeToDestroy -= Time.deltaTime;
        //This checks the distance between the player and the enemy
        Vector3 vectorToTarget = player.transform.position - transform.position;

        if (vectorToTarget.magnitude < 1f) //This checks if the target has hit the enemy.
        {
            Destroy(gameObject);
            player.TakeDamage(damageDealt);
        }
        if(timeToDestroy <= 0)
        {
            Destroy(gameObject);
        }

        //this moves the fireball towards the enemy.
        if(timeToFollow > 0f)
        {
            Quaternion targetRotation = Quaternion.LookRotation(vectorToTarget);
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, trackingSpeed * Time.deltaTime);
            timeToFollow -= Time.deltaTime;
        }
        transform.position += transform.forward * Time.deltaTime * moveSpeed; 
    }
}
