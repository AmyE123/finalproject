﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

[System.Serializable]
public class timingData
{
    public List <float> blackTime = new List<float>();
    public List <float> whiteTime = new List<float>();
}

public class InputRecorder : MonoBehaviour
{
    public AudioSource audioSource;
    public timingData timeData;

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Z))
        {
            timeData.whiteTime.Add(audioSource.time);
        }
        if(Input.GetKeyDown(KeyCode.X))
        {
            timeData.blackTime.Add(audioSource.time);
        }
        if(Input.GetKeyDown(KeyCode.S))
        {
            SaveToFile();
        }
    }

    public void SaveToFile()
    {
        string jsonString = JsonUtility.ToJson(timeData);
        StreamWriter writer = new StreamWriter ("Assets/Data/musicTiming.txt");
        writer.WriteLine (jsonString);
        writer.Close();
    }
}
