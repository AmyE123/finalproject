﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Ring : MonoBehaviour
{
    public float moveSpeed;
    public float currentScale = 0.5f;
    public AudioClip ringSound;
    public AudioClip wrongSound;
    public AudioSource bellSound;

    public int ringColour;

    void Awake()
    {
        var audioObj = GameObject.Find("BellSound");
        bellSound = audioObj.GetComponent<AudioSource>();
    }

    public void PlayBell()
    {
        bellSound.Stop();
        bellSound.clip = ringSound;
        bellSound.Play();
        //bellSound.PlayOneShot(ringSound);
    }
    public void PlayWrong()
    {
        bellSound.Stop();
        bellSound.clip = wrongSound;
        bellSound.Play();
        //bellSound.PlayOneShot(ringSound);
    }

    void Update()
    {
        currentScale += Time.deltaTime * moveSpeed;
        transform.localScale = new Vector3(currentScale, currentScale, 1);
        if(currentScale > 18)
        {
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        RingOut otherRing = other.gameObject.GetComponent<RingOut>();
        if(otherRing != null)
        {
            if(otherRing.ringColourOut == this.ringColour)
            {
                Debug.Log("Correct");
                PlayBell();
                Destroy(gameObject);
                Destroy(other.gameObject);
            }
            else
            {
                Debug.Log("Incorrect");
                PlayWrong();
                Destroy(other.gameObject);
                otherRing.increaseDelay();
            }
        }
        if(other.tag == "Player")
        {
            Debug.Log("Dead");
            Scene scene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(scene.name);
        }
    }
}
