﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RingOut : MonoBehaviour
{
    public float moveSpeed;
    public float currentScale = 0.5f;
    public AudioClip playerRingClip;
    public AudioSource playerRing;
    public int ringColourOut;

    public RingOutSpawner spawner;
    public float delayTime;

    void Awake()
    {
        var audioObj = GameObject.Find("PlayerRing");
        playerRing = audioObj.GetComponent<AudioSource>();
    }

    void Start()
    {
        playerRing.Stop();
        playerRing.clip = playerRingClip;
        playerRing.Play();
    }

    void Update()
    {
        currentScale += Time.deltaTime * moveSpeed;
        transform.localScale = new Vector3(currentScale, currentScale, 1);
    }

    public void increaseDelay()
    {
        spawner.delay = delayTime;
    }
}
