﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TargetingSystem : MonoBehaviour
{
    public CrosshairManager crosshairManager;
    public float maxDistance;
    public Transform player;

    [Header("Enemy properties")]
    public EnemyManager enemyManager;
    public List<EnemyScript> targetedEnemies = new List<EnemyScript>();

    [Header("Fireball & targeting properties")]
    public GameObject fireBallPrefab;
    public Transform fireballSpawner;
    public int targetLimit;

    [Header("Timer properties")]
    public float coolDownTimer = 2;
    public bool coolingDown = false;

    [Header("Frame delay timer properties")]
    public float frameDelayCooldown = 0.05f;

    [Header("SoundFX")]
    public AudioClip targetingSound;
    public AudioSource soundFx;

    [Header("Feedback")]
    public Color32 activeCrosshairColour;
    public Color32 inactiveCrosshairColour;
    public Color32 targetingCrosshairColour;
    public Image aimCrosshair;
    public Sprite activeCrosshair;
    public Sprite inactiveCrosshair;
    public Sprite targetingCrosshair;

    [Header("AnimProperties")]
    public Animator anim;

    Camera cam;

    //This whole function checks if an enemy is on screen
    public bool IsOnScreen(EnemyScript enemy)
    {
        Vector3 worldPos = enemy.transform.position;
        Vector3 screenPos = cam.WorldToViewportPoint(worldPos);
        if (screenPos.x < 0 || screenPos.x > 1)
        {
            return false;
        } 
        if (screenPos.y < 0 || screenPos.y > 1)
        {
            return false;
        }
        if (screenPos.z < 0)
        {
            return false;
        }
        return true;
    }

    //This whole function checks if the enemy is on the centre of..
    //..the screen which is where the player is aiming from.
    public bool IsOnCrosshair(EnemyScript enemy)
    {
        Vector3 worldPos = enemy.transform.position;
        Vector3 screenPos = cam.WorldToViewportPoint(worldPos);
        Vector2 crosshairPosition = crosshairManager.GetCrosshairPosition();
        if (screenPos.x < crosshairPosition.x - 0.1f || screenPos.x > crosshairPosition.x + 0.1f)
        {
            return false;
        }
        if (screenPos.y < crosshairPosition.y - 0.2f || screenPos.y > crosshairPosition.y + 0.2f)
        {
            return false;
        }
        if (screenPos.z < 0)
        {
            return false;
        }
        return true;
    }

    void ScanForTargets()
    {
        //This grabs every enemy we have and runs a for each loop on them
        foreach (EnemyScript e in enemyManager.allEnemies)
        {
            float distance = Vector3.Distance(player.position, e.transform.position);
            //if one particular enemy meets all the requirements they're added to the..
            //..targeted enemies list.
            if (IsOnCrosshair(e) && !targetedEnemies.Contains(e) && targetedEnemies.Count < targetLimit && frameDelayCooldown <= 0 && distance <= maxDistance)
            {
                targetedEnemies.Add(e);
                soundFx.PlayOneShot(targetingSound, 1f);
                frameDelayCooldown = 0.05f;
            }
        }
        frameDelayCooldown -= Time.deltaTime;
    }

    //This function instanciates the fireballs which we have set..
    //..and they're instanciated at the position we set in an empty child..
    //..on the player character.
    void FireAtTargets()
    {
        foreach (EnemyScript e in targetedEnemies)
        {
            GameObject newFireball = Instantiate(fireBallPrefab);
            newFireball.transform.position = fireballSpawner.position;
            newFireball.GetComponent<Fireball>().enemy = e;
        }
        targetedEnemies = new List<EnemyScript>();
        anim.SetTrigger("Attack");
    }

    void Start()
    {
        cam = Camera.main;
    }


    void Update()
    {
        if (coolingDown == false)
        {
            aimCrosshair.sprite = activeCrosshair;
            aimCrosshair.color = activeCrosshairColour;
            
            if (Input.GetButton("Square") || Input.GetKey(KeyCode.LeftShift) || Input.GetMouseButton(0))
            {
                aimCrosshair.sprite = targetingCrosshair;
                aimCrosshair.color = targetingCrosshairColour;
                ScanForTargets();  
            }
            if (Input.GetButtonUp("Square") || Input.GetKeyUp(KeyCode.LeftShift) || Input.GetMouseButtonUp(0))
            {
                FireAtTargets();
                coolingDown = true;
                coolDownTimer = 2;
                //PostProcessing
            }
        }
        else
        {
            coolDownTimer -= Time.deltaTime;
            aimCrosshair.sprite = inactiveCrosshair;
            aimCrosshair.color = inactiveCrosshairColour;
            
            if (coolDownTimer <= 0)
            {
                coolDownTimer = 0;
                coolingDown = false;
            }
        }

    }
}
