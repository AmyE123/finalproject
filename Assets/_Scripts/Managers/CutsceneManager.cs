﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CutsceneManager : MonoBehaviour
{
    public GameObject cutscene_00;
    public GameObject cutscene_01;
    public GameObject cutscene_02;
    public GameObject cutscene_03;
    public GameObject cutscene_04;

    public CutsceneCounter cutsceneCounter;
    public CutsceneDetectDone cutsceneDetectDone;

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.P))
        {
            cutsceneDetectDone.isDonePlaying = true;
        }
        if (cutsceneCounter.Count == 0)
        {
            cutscene_00.SetActive(true);
            cutsceneDetectDone = cutscene_00.GetComponent<CutsceneDetectDone>();
            if (cutsceneDetectDone.isDonePlaying == true)
            {
                Debug.Log("Done");
                SceneManager.LoadScene("MainMenu");
            }
        }
        if (cutsceneCounter.Count == 1)
        {
            Debug.Log("Cutscene_01");
            cutscene_01.SetActive(true);
            cutsceneDetectDone = cutscene_01.GetComponent<CutsceneDetectDone>();
            if (cutsceneDetectDone.isDonePlaying == true)
            {
                SceneManager.LoadScene("Level_Order");
            }
        }
        if (cutsceneCounter.Count == 2)
        {
            cutscene_02.SetActive(true);
            cutsceneDetectDone = cutscene_02.GetComponent<CutsceneDetectDone>();
            if (cutsceneDetectDone.isDonePlaying == true)
            {
                SceneManager.LoadScene("Level_Chaos");
            }
        }
        if(cutsceneCounter.Count > 2)
        {
            SceneManager.LoadScene("Level_X");
        }
        if (cutsceneCounter.Count == 3)
        {
            cutscene_03.SetActive(true);
            cutsceneDetectDone = cutscene_03.GetComponent<CutsceneDetectDone>();
            if (cutsceneDetectDone.isDonePlaying == true)
            {
                //SceneManager.LoadScene("Level_OrderChaos");
            }
        }
        if (cutsceneCounter.Count == 4)
        {
            cutscene_04.SetActive(true);
            cutsceneDetectDone = cutscene_04.GetComponent<CutsceneDetectDone>();
            if (cutsceneDetectDone.isDonePlaying == true)
            {
                //Done Game
            }
        }
    }
}
