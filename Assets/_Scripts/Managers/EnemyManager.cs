﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    //This is a list which stores all the enemies
    public List<EnemyScript> allEnemies = new List<EnemyScript>();

    //this static EnemyManager makes it so you can access the enemy manager anywhere
    //and so theres only one Instance of it.
    public static EnemyManager instance;

    //This function makes the integer EnemyCount accessable from anywhere
    public static int EnemyCount()
    {
        //every time this function is called, the enemyCount is returned
        return(instance.allEnemies.Count);
    }

    void Awake()
    {
        //this defines itself as the enemy manager
        instance = this;
    }
    
    //This function adds (Registers) the enemies to the EnemyManager
    public static void Register(EnemyScript e)
    {
        instance.allEnemies.Add(e);
        //This sets the enemies as a child of the EnemyManager.
        e.transform.SetParent(instance.transform);
    }

    //This function removes (Deregisters) the enemies from the EnemyManager.
    public static void Deregister(EnemyScript e)
    {
        instance.allEnemies.Remove(e);
    }
}
